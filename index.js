var net = require('net'),
    JsonSocket = require('json-socket'),
    printer = require('printer'),
    fs = require('fs');
const EventEmitter = require('events').EventEmitter;


var port = 3002;
var server = net.createServer();
var emitter = new EventEmitter;

var Datastore = require('nedb')
    , db = new Datastore({ filename: 'tasks.db' });

var reprintedOrders = [];

db.loadDatabase(function (err) {
    if (err) throw err;
    console.log('База данных загружена!')

    let currentJobs = printer.getPrinter(printer.getDefaultPrinterName()).jobs;
    if (currentJobs) {
        console.log('Задач в принтере:', currentJobs.length)
        console.log('Отчистка текущих задач в принтере..')
        currentJobs.forEach((job, index, array) => {
            printer.setJob(printer.getDefaultPrinterName(), job.id, 'CANCEL')
            if (index === array.length) console.log('Отчистка завершена!')
        })
    } else console.log('Задачи в принтере не обнаружены!')

    getUnprintedTasks().then(tasks => {
        if (!tasks || tasks.length === 0) {
            console.log('Задачи в очереди не обнаружены!');
            return;
        }

        console.log('Задач в очереди:', tasks)
        console.log('Печать очереди..')

        tasks.forEach((task, index, array) => {
            fs.readFile(task.path, (err, buf) => {
                if (err) throw err;
                printPDF(buf, id => {
                    console.log('Success!')
                    db.update({ orderId: task.orderId }, { $set: { jobId: id } }, {}, (err, upsert) => { if (err) throw err; })
                }, (id, state) => {
                    console.log('Update! ', state)
                }, (id, state) => {
                    console.log('Printed! ', state, task.orderId)
                    db.update({ orderId: task.orderId }, { $set: { printed: true } }, {}, (err, upsert) => {
                        if (err) throw err;
                        reprintedOrders.push({ orderId: task.orderId, customerId: task.customerId, status: state })
                        emitter.emit('order_reprinted', { reprintedOrders });
                    })
                }, (err) => {
                    db.update({ orderId: message.orderId }, { $set: { err: err } }, {}, (err, upsert) => { if (err) throw err; })
                })

                if (index === array.length) console.log('Печать завершена!')
            })
        })
    })
});

let express = require('express');
let app = express();

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/node_modules/bootstrap'));

app.get('/', (req, res) => {
    getTasks().then(tasks => {
        res.render('index.ejs', { tasks: tasks });
    })
});

function printPDF(buffer, onSuccess, onUpdated, onPrinted, onFailed) {
    return printer.printDirect({
        data: buffer, type: 'PDF', success: id => {
            onSuccess(id)
            let oldStatus = '';
            let interval = setInterval(function () {
                let status = printer.getJob(printer.getDefaultPrinterName(), id).status[0];
                if (status !== oldStatus) {
                    if (status === 'PRINTED') {
                        onPrinted(id, status)
                        clearInterval(interval);
                    }
                    else {
                        onUpdated(id, status)
                        oldStatus = status;
                    }
                }
            }, 1000);
        }, error: function (err) {
            onFailed(err)
        }
    });
}

app.listen(4000, () => console.log('Example app listening on port 4000!'));

server.listen(port, () => {
    console.log('Server is running!')
});

function getUnprintedTasks() {
    return new Promise((resolve, reject) => {
        db.find({ printed: false }, (err, docs) => {
            if (err) reject(err);
            else resolve(docs);
        });
    });
}

function getTasks() {
    return new Promise((resolve, reject) => {
        db.find({}, (err, docs) => {
            if (err) reject(err);
            else resolve(docs);
        });
    });
}

function getTask(orderId, customerId) {
    return new Promise((resolve, reject) => {
        db.findOne({ orderId, customerId }, (err, doc) => {
            if (err) reject(err);
            else resolve(doc);
        });
    });
}

server.on('connection', function (socket) {
    console.log('Установлено соединение с сервером!');
    // TODO: авторизация

    if (reprintedOrders && reprintedOrders.length > 0) {
        console.log('Отправка восстановленных заказов..')

        reprintedOrders.forEach((order, index, array) => {
            socket.sendMessage(order, (err) => {
                if (err) throw err;
                array.splice(index, 1);
            });
        })
    }

    emitter.on('order_reprinted', (data => {
        data.reprintedOrders.forEach((order, index, array) => {
            socket.sendMessage(order, (err) => {
                if (err) throw err;
                array.splice(index, 1);
            });
        })
    }))

    socket.on('end', socket => {
        console.log('Соединение с сервером потеряно!')
    })

    socket = new JsonSocket(socket);
    socket.on('message', function (message) {
        getTask(message.orderId, message.customerId).then(task => {
            if (task === null) {
                let buf = Buffer.from(message.buf);
                let path = `${__dirname}/docs/${message.customerId}_${message.orderId}`;
                fs.writeFile(path, buf, err => {
                    if (err) throw err;
                    db.insert({
                        orderId: message.orderId,
                        customerId: message.customerId,
                        path: path,
                        printed: false,
                        jobId: -1,
                        err: {}
                    }, (err, doc) => {
                        if (err) throw err;
                        printPDF(buf, id => {
                            console.log('Success! Job started for', id)
                            db.update({ orderId: message.orderId }, { $set: { jobId: id } }, {}, (err, upsert) => { if (err) throw err; })
                        }, (id, state) => {
                            console.log('Update for ', id, 'is', state)
                            socket.sendMessage({ orderId: message.orderId, customerId: message.customerId, status: state })
                        }, (id, state) => {
                            console.log(`${id} is printed!`)
                            db.update({ orderId: message.orderId }, { $set: { printed: true } }, {}, (err, upsert) => {
                                socket.sendMessage({ orderId: message.orderId, customerId: message.customerId, status: state })
                                if (err) throw err;
                            })
                        }, (err) => {
                            console.error(err);
                            db.update({ orderId: message.orderId }, { $set: { err: err } }, {}, (err, upsert) => { if (err) throw err; })
                        })
                    });
                })
            } else if (task.printed) {
                socket.sendMessage({ orderId: message.orderId, customerId: message.customerId, status: 'PRINTED' })
            }
        }).catch(err => {
            if (err) throw err;
        })
    });
});

